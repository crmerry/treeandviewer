#pragma once

namespace BINARY_TREE
{
	typedef int DATA_TYPE;

	struct Node
	{
		Node*		_left = nullptr;
		Node*		_right = nullptr;
		DATA_TYPE	_data;
		int			_x;
		int			_y;
	};

	class BinaryTree
	{
	public:
		BinaryTree();
		~BinaryTree();
		bool InsertNode(DATA_TYPE data);
		void PreorderPrint();
		void InorderPrint();
		void PostorderPrint();
		void Release();
		bool Search(Node** node, DATA_TYPE data);
		bool Delete(DATA_TYPE data);
		Node* GetRoot();
		Node* GetLeft(Node* node);
		Node* GetRight(Node* node);
	
	protected:
		Node* CreateNode(DATA_TYPE data);
		bool LinkNodeToNodeByRecursive(Node** parent_node, Node* new_node);
		void PreorderPrintByRecursive(Node* next, int depth = 0);
		void InorderPrintByRecursive(Node* next, int depth = 0);
		void PostorderPrintByRecursive(Node* next, int depth = 0);
		void PrintDataByDepth(Node* next, int depth);
		void ReleaseByRecursive(Node* next);
		bool SearchByRecursive(Node** target, Node** prev, Node* next, DATA_TYPE data);
		bool DeleteByLoop(Node** target, Node** prev);
	
	private:
		Node* _root;
	};
}
