#include "stdafx.h"
#include "binary_tree.h"

using namespace BINARY_TREE;

BinaryTree::BinaryTree()
	:_root(nullptr)
{

}

BinaryTree::~BinaryTree()
{
	ReleaseByRecursive(_root);
	_root = nullptr;
}

bool BinaryTree::InsertNode(DATA_TYPE data)
{
	Node* new_node = CreateNode(data);

	if (!_root)
	{
		_root = new_node;

		return true;
	}

	if (!LinkNodeToNodeByRecursive(&_root, new_node))
	{
		delete new_node;

		return false;
	}

	return true;
}

void BinaryTree::PreorderPrint()
{
	PreorderPrintByRecursive(_root);
}

void BinaryTree::InorderPrint()
{
	InorderPrintByRecursive(_root);
}

void BinaryTree::PostorderPrint()
{
	PostorderPrintByRecursive(_root);
}

void BinaryTree::Release()
{
	ReleaseByRecursive(_root);
	_root = nullptr;
}

bool BINARY_TREE::BinaryTree::Search(Node** target, DATA_TYPE data)
{	
	Node* prev;

	return SearchByRecursive(target, &prev, _root, data);
}

bool BINARY_TREE::BinaryTree::Delete(DATA_TYPE data)
{
	Node* target = nullptr;
	Node* prev = nullptr;

	if (SearchByRecursive(&target, &prev, _root, data))
	{
		return DeleteByLoop(&target, &prev);
	}
	else
	{
		return false;
	}
}

Node* BINARY_TREE::BinaryTree::GetRoot()
{
	return _root;
}

Node* BINARY_TREE::BinaryTree::GetLeft(Node* node)
{
	return node->_left;
}

Node* BINARY_TREE::BinaryTree::GetRight(Node* node)
{
	return node->_right;
}

Node* BinaryTree::CreateNode(DATA_TYPE data)
{
	Node* created_node = new Node;
	created_node->_data = data;

	return created_node;
}

bool BinaryTree::LinkNodeToNodeByRecursive(Node** parent_node, Node* new_node)
{
	if (!(*parent_node))
	{
		*parent_node = new_node;

		return true;
	}

	if ((*parent_node)->_data > new_node->_data)
	{
		return LinkNodeToNodeByRecursive(&(*parent_node)->_left, new_node);
	}
	else if ((*parent_node)->_data < new_node->_data)
	{
		return LinkNodeToNodeByRecursive(&(*parent_node)->_right, new_node);
	}
	else
	{
		assert(0);

		return false;
	}
}

void BinaryTree::PreorderPrintByRecursive(Node* next, int depth)
{
	if (!next)
	{
		return;
	}

	PrintDataByDepth(next, depth);
	PreorderPrintByRecursive(next->_left, depth + 1);
	PreorderPrintByRecursive(next->_right, depth + 1);
}

void BinaryTree::InorderPrintByRecursive(Node* next, int depth)
{
	if (!next)
	{
		return;
	}

	InorderPrintByRecursive(next->_left, depth + 1);
	PrintDataByDepth(next, depth);
	InorderPrintByRecursive(next->_right, depth + 1);
}

void BinaryTree::PostorderPrintByRecursive(Node* next, int depth)
{
	if (!next)
	{
		return;
	}

	PostorderPrintByRecursive(next->_left, depth + 1);
	PostorderPrintByRecursive(next->_right, depth + 1);
	PrintDataByDepth(next, depth);
}

void BinaryTree::PrintDataByDepth(Node* next, int depth)
{
	for (int index = 0; index < depth; index++)
	{
		printf(" ");
	}
	printf("%d\n", next->_data);
}

void BinaryTree::ReleaseByRecursive(Node* next)
{
	if (next == nullptr)
	{
		return;
	}

	ReleaseByRecursive(next->_left);
	ReleaseByRecursive(next->_right);

	delete next;
}

bool BINARY_TREE::BinaryTree::SearchByRecursive(Node** target, Node** prev, Node* next, DATA_TYPE data)
{
	if (!next)
	{
		return false;
	}

	if (next->_data > data)
	{
		if (SearchByRecursive(target, prev, next->_left, data))
		{
			if (!*prev)
			{
				*prev = next;
			}
			return true;
		}
	}
	else if (next->_data < data)
	{
		if (SearchByRecursive(target, prev, next->_right, data))
		{
			if (!*prev)
			{
				*prev = next;
			}

			return true;
		}
	}
	else
	{
		*target = next;

		return true;
	}

	return false;
}

bool BINARY_TREE::BinaryTree::DeleteByLoop(Node** target, Node** prev)
{
	Node* nearest = *target;
	Node* nearest_parent = *prev;

	if (nearest->_left)
	{
		nearest_parent = nearest;
		nearest = nearest->_left;

		while (1)
		{
			if (nearest->_right)
			{
				nearest_parent = nearest;
				nearest = nearest->_right;
			}
			else
			{
				break;
			}
		}

		if (*target == _root)
		{
			if (nearest_parent == *target)
			{
				nearest->_right = (*target)->_right;
				_root = nearest;

				delete *target;

				return true;
			}

			nearest_parent->_right = nearest->_left;
			nearest->_left = (*target)->_left;
			nearest->_right = (*target)->_right;
			_root = nearest;

			delete *target;

			return true;
		}

		if (nearest_parent == *target)
		{
			if ((*prev)->_left == *target)
			{
				(*prev)->_left = nearest;
				nearest->_right = (*target)->_right;

				delete *target;

				return true;
			}
			else if ((*prev)->_right == *target)
			{
				(*prev)->_right = nearest;
				nearest->_right = (*target)->_right;

				delete *target;

				return true;
			}
		}

		nearest_parent->_right = nearest->_left;
		nearest->_left = (*target)->_left;
		nearest->_right = (*target)->_right;

		if ((*prev)->_left == *target)
		{
			(*prev)->_left = nearest;
		}
		else if ((*prev)->_right == *target)
		{
			(*prev)->_right = nearest;
		}

	}
	else if (nearest->_right)
	{
		if (nearest == _root)
		{
			_root = nearest->_right;

			delete nearest;

			return true;
		}

		(*prev)->_right = (*target)->_right;

		delete nearest;

		return true;
	}
	else
	{
		if (*prev)
		{
			if ((*prev)->_left == *target)
			{
				(*prev)->_left = nullptr;
			}
			else if ((*prev)->_right == *target)
			{
				(*prev)->_right = nullptr;
			}

			delete *target;
		}
		else
		{
			delete *target;

			_root = nullptr;
		}
	}
	return false;
}
