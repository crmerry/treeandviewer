#include "stdafx.h"
#include "TreeViewer.h"
#include "binary_tree.h"

#define MAX_LOADSTRING 100

// 전역 변수:
HINSTANCE hInst;                                // 현재 인스턴스입니다.
BINARY_TREE::BinaryTree g_tree;
HWND	g_hWnd;
HWND	g_hMDlg;
int g_x;
int g_prev_x;
int g_prev_y;
HDC g_mem_dc;
int g_screen_x;
int g_screen_y;

// 이 코드 모듈에 들어 있는 함수의 정방향 선언입니다.
LRESULT CALLBACK    WndProc(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK    About(HWND, UINT, WPARAM, LPARAM);

void InOrderTraversalByRecursive(BINARY_TREE::Node* next, int depth)
{
	if (!next)
	{
		return;
	}

	InOrderTraversalByRecursive(next->_left, depth + 75);
	
	Ellipse(g_mem_dc, g_x, depth, g_x + 50, depth + 50);

	wchar_t data[10];
	memset(data, 0, sizeof data);
	swprintf_s(data, 10, L"%d", next->_data);
	TextOut(g_mem_dc, g_x + 15, depth + 15, data, (int)wcslen(data));

	next->_x = g_x;
	next->_y = depth;

	g_x += 30;

	InOrderTraversalByRecursive(next->_right, depth + 75);
}

void PreOrderTraversalByRecursive(BINARY_TREE::Node* next)
{
	if (!next)
	{
		return;
	}

	if (next->_left)
	{
		MoveToEx(g_mem_dc, next->_x + 25, next->_y + 50, NULL);
		LineTo(g_mem_dc, next->_left->_x + 25, next->_left->_y);

		PreOrderTraversalByRecursive(next->_left);
	}

	if (next->_right)
	{
		MoveToEx(g_mem_dc, next->_x + 25, next->_y + 50, NULL);
		LineTo(g_mem_dc, next->_right->_x + 25, next->_right->_y);

		PreOrderTraversalByRecursive(next->_right);
	}
}

void Test3()
{
	g_x = 0;
	g_prev_x = 0;
	g_prev_y = 0;

	HDC hdc = GetDC(g_hWnd);

	g_mem_dc = CreateCompatibleDC(hdc);
	HBITMAP bitmap_current = CreateCompatibleBitmap(hdc, 3840, 2160);
	HBITMAP bitmap_old = (HBITMAP)SelectObject(g_mem_dc, bitmap_current);
	
	HBRUSH back_color = CreateSolidBrush(RGB(51, 184, 165));
	HBRUSH old_back =(HBRUSH)SelectObject(g_mem_dc, back_color);
	Rectangle(g_mem_dc, 0, 0, 3840, 2160);
	
	HBRUSH circle_color = CreateSolidBrush(RGB(240, 73, 80));
	SelectObject(g_mem_dc, circle_color);
	
	SetTextColor(g_mem_dc, RGB(255, 255, 255));
	SetBkColor(g_mem_dc, RGB(240, 73, 80));
	
	HPEN pen = CreatePen(PS_SOLID, 2, RGB(255, 255, 255));
	HPEN old_pen = (HPEN)SelectObject(g_mem_dc, pen);
	
	DeleteDC(hdc);

	InOrderTraversalByRecursive(g_tree.GetRoot(), 0);
	PreOrderTraversalByRecursive(g_tree.GetRoot());

	hdc = GetDC(g_hWnd);

	BitBlt(hdc, 0, 0,1920, 1080, g_mem_dc, g_screen_x, g_screen_y, SRCCOPY);
	
	DeleteDC(hdc);

	SelectObject(g_mem_dc, old_pen);
	SelectObject(g_mem_dc, old_back);
	DeleteDC(g_mem_dc);
}

int APIENTRY wWinMain(_In_ HINSTANCE hInstance,
                     _In_opt_ HINSTANCE hPrevInstance,
                     _In_ LPWSTR    lpCmdLine,
                     _In_ int       nCmdShow)
{
    UNREFERENCED_PARAMETER(hPrevInstance);
    UNREFERENCED_PARAMETER(lpCmdLine);

    // TODO: 여기에 코드를 입력합니다.
	WNDCLASSEXW wcex;

	wcex.cbSize = sizeof(WNDCLASSEX);

	wcex.style = CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc = WndProc;
	wcex.cbClsExtra = 0;
	wcex.cbWndExtra = 0;
	wcex.hInstance = hInstance;
	wcex.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_TREEVIEWER));
	wcex.hCursor = LoadCursor(nullptr, IDC_ARROW);
	wcex.hbrBackground = (HBRUSH)(COLOR_WINDOW + 1);
	wcex.lpszMenuName = MAKEINTRESOURCEW(IDC_TREEVIEWER);
	wcex.lpszClassName = L"g_treeViewer";
	wcex.hIconSm = LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));

	RegisterClassExW(&wcex);

	hInst = hInstance; // 인스턴스 핸들을 전역 변수에 저장합니다.

	HWND hWnd = CreateWindowW(L"g_treeViewer", L"BinaryTreeViewer", WS_OVERLAPPEDWINDOW,
		CW_USEDEFAULT, 0, CW_USEDEFAULT, 0, nullptr, nullptr, hInstance, nullptr);
	g_hWnd = hWnd;	
	if (!hWnd)
	{
		return FALSE;
	}

	ShowWindow(hWnd, nCmdShow);
	UpdateWindow(hWnd);

    // 응용 프로그램 초기화를 수행합니다.

    MSG msg;

    // 기본 메시지 루프입니다.
    while (GetMessage(&msg, nullptr, 0, 0))
    {
        TranslateMessage(&msg);
        DispatchMessage(&msg);
    }

    return (int) msg.wParam;
}

INT_PTR CALLBACK MlessDlgProc(HWND hDlg, UINT iMessage, WPARAM wParam, LPARAM lParam)
{
	switch (iMessage)
	{
	case WM_INITDIALOG:
	{
		int val = rand() % 500;
		SetDlgItemInt(hDlg, IDC_EDIT3, val, true);
	}
		break;

	case WM_COMMAND:
		switch (LOWORD(wParam))
		{
		case IDC_BUTTON1:
		{
			wchar_t value[10];
			GetDlgItemText(hDlg, IDC_EDIT1, value, sizeof value / sizeof(wchar_t));
		
			int val = _wtoi(value);
			g_tree.InsertNode(val);
			
			InvalidateRect(g_hWnd, NULL, false);

			return true;
		}
		case IDC_BUTTON2:
		{
			wchar_t value[10];
			GetDlgItemText(hDlg, IDC_EDIT2, value, sizeof value / sizeof(wchar_t));
			
			int val = _wtoi(value);
			g_tree.Delete(val);
			
			InvalidateRect(g_hWnd, NULL, false);

			return true;
		}
		case IDC_BUTTON3:
		{	
			wchar_t value[10];
			GetDlgItemText(hDlg, IDC_EDIT3, value, sizeof value / sizeof(wchar_t));

			int val = _wtoi(value);
			g_tree.InsertNode(val);
			
			val = rand() % 500;
			SetDlgItemInt(hDlg, IDC_EDIT3, val, true);

			InvalidateRect(g_hWnd, NULL, false);
			
			return true;
		}
		case IDC_BUTTON5:
		{
			g_tree.Release();

			InvalidateRect(g_hWnd, NULL, false);

			return true;
		}
		default:
			break;
		}
	default:
		break;
	}

	return false;
}

//
//  함수: WndProc(HWND, UINT, WPARAM, LPARAM)
//
//  목적:  주 창의 메시지를 처리합니다.
//
//  WM_COMMAND  - 응용 프로그램 메뉴를 처리합니다.
//  WM_PAINT    - 주 창을 그립니다.
//  WM_DESTROY  - 종료 메시지를 게시하고 반환합니다.
//
//
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
    switch (message)
    {
    case WM_COMMAND:
        {
            int wmId = LOWORD(wParam);
            // 메뉴 선택을 구문 분석합니다.
            switch (wmId)
            {
            case IDM_ABOUT:
                DialogBox(hInst, MAKEINTRESOURCE(IDD_ABOUTBOX), hWnd, About);
                break;
            case IDM_EXIT:
                DestroyWindow(hWnd);
                break;
            default:
                return DefWindowProc(hWnd, message, wParam, lParam);
            }
        }
        break;
    case WM_PAINT:
        {
            PAINTSTRUCT ps;
            HDC hdc = BeginPaint(hWnd, &ps);
            EndPaint(hWnd, &ps);
			
			Test3();
        }
        break;
	case WM_KEYDOWN:
	{
		switch (wParam)
		{
		case VK_LEFT:
			g_screen_x -= 10;
			g_screen_x = max(g_screen_x, 0);
			break;
		case VK_RIGHT:
			g_screen_x += 10;
			g_screen_x = min(3840, g_screen_x);
			break;
		case VK_UP:
			g_screen_y -= 10;
			g_screen_y = max(g_screen_y, 0);
			break;
		case VK_DOWN:
			g_screen_y += 10;
			g_screen_x = min(2160, g_screen_x);
			break;
		default:
			break;
		}
		InvalidateRect(g_hWnd, NULL, false);
	}
		break;
	case WM_CREATE:
		if (!IsWindow(g_hMDlg))
		{
			g_hMDlg = CreateDialog(hInst, MAKEINTRESOURCE(IDD_DIALOG1), hWnd, MlessDlgProc);
			ShowWindow(g_hMDlg, SW_SHOW);
		}
		break;
    case WM_DESTROY:
        PostQuitMessage(0);
        break;
    default:
        return DefWindowProc(hWnd, message, wParam, lParam);
    }
    return 0;
}

// 정보 대화 상자의 메시지 처리기입니다.
INT_PTR CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
    UNREFERENCED_PARAMETER(lParam);
    switch (message)
    {
    case WM_INITDIALOG:
        return (INT_PTR)TRUE;

    case WM_COMMAND:
        if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)
        {
            EndDialog(hDlg, LOWORD(wParam));
            return (INT_PTR)TRUE;
        }
        break;
    }
    return (INT_PTR)FALSE;
}
