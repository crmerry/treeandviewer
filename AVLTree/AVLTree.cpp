// AVLTree.cpp : 응용 프로그램에 대한 진입점을 정의합니다.
//

#include "stdafx.h"
#include "AVLTree.h"
#include "my_new.h"
/* ***************************************
	매크로
**************************************** */
#define MAX_LOADSTRING 100

/* ***************************************   
	구조체
**************************************** */
typedef int DATA_TYPE;

struct Node
{
	Node*		_left;
	Node*		_right;

	DATA_TYPE	_data;
	int			_x;
	int			_y;
};

/* ***************************************
	함수
**************************************** */
void InOrderTraversalByRecursive(Node* next, int depth);
void PreOrderTraversalByRecursive(Node* next);
void Test3(HDC hdc);

void Insert(Node** node, DATA_TYPE data);
Node* Rebalance(Node* node);
Node* SearchNode(Node* node, DATA_TYPE data);
void Delete(Node** node, DATA_TYPE data);
void DeleteByRecursive(Node** node, DATA_TYPE data, Node* target);
void ReleaseByRecursive(Node* next);
Node* RotateRR(Node* node);
Node* RotateLL(Node* node);
Node* RotateRL(Node* node);
Node* RotateLR(Node* node);
int CalculateDepth(Node* node);
int CalculateBalance(Node* node);
LRESULT CALLBACK    WndProc(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK    About(HWND, UINT, WPARAM, LPARAM);

/* ***************************************
	전역 변수
**************************************** */
HINSTANCE hInst;                                // 현재 인스턴스입니다.
HWND	g_hMDlg;
HWND	g_hWnd;
Node*	g_root = nullptr;
HDC		g_mem_dc = nullptr;
int		g_x;
int		g_y;
int		g_prev_x;
int		g_prev_y;
int		g_screen_x;
int		g_screen_y;

int APIENTRY wWinMain(_In_ HINSTANCE hInstance,
	_In_opt_ HINSTANCE hPrevInstance,
	_In_ LPWSTR    lpCmdLine,
	_In_ int       nCmdShow)
{
	UNREFERENCED_PARAMETER(hPrevInstance);
	UNREFERENCED_PARAMETER(lpCmdLine);

	/* ***************************************
		윈도우 등록
	**************************************** */
	{
		WNDCLASSEXW wcex;

		wcex.cbSize = sizeof(WNDCLASSEX);

		wcex.style = CS_HREDRAW | CS_VREDRAW;
		wcex.lpfnWndProc = WndProc;
		wcex.cbClsExtra = 0;
		wcex.cbWndExtra = 0;
		wcex.hInstance = hInstance;
		wcex.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_AVLTREE));
		wcex.hCursor = LoadCursor(nullptr, IDC_ARROW);
		wcex.hbrBackground = (HBRUSH)(COLOR_WINDOW + 1);
		wcex.lpszMenuName = MAKEINTRESOURCEW(IDC_AVLTREE);
		wcex.lpszClassName = L"AVLTree";
		wcex.hIconSm = LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));

		RegisterClassExW(&wcex);
	}

	/* ***************************************
		윈도우 생성 및 보여주기
	**************************************** */
	{
		hInst = hInstance; // 인스턴스 핸들을 전역 변수에 저장합니다.

		HWND hWnd = CreateWindowW(L"AVLTree", L"AVLTree Title", WS_OVERLAPPEDWINDOW,
			CW_USEDEFAULT, 0, CW_USEDEFAULT, 0, nullptr, nullptr, hInstance, nullptr);

		if (!hWnd)
		{
			return FALSE;
		}
		g_hWnd = hWnd;

		ShowWindow(hWnd, nCmdShow);
		UpdateWindow(hWnd);
	}
	
	MSG msg;

    // 기본 메시지 루프입니다.
    while (GetMessage(&msg, nullptr, 0, 0))
    {
        TranslateMessage(&msg);
        DispatchMessage(&msg);
    }

    return (int) msg.wParam;
}

INT_PTR CALLBACK MlessDlgProc(HWND hDlg, UINT iMessage, WPARAM wParam, LPARAM lParam)
{
	switch (iMessage)
	{
	case WM_INITDIALOG:
		{
			int val = rand() % 500;
			SetDlgItemInt(hDlg, IDC_EDIT3, val, true);
		}
		break;

	case WM_COMMAND:
		switch (LOWORD(wParam))
		{
		case IDC_BUTTON1:
			{
				wchar_t value[10];
				GetDlgItemText(hDlg, IDC_EDIT1, value, sizeof value / sizeof(wchar_t));

				int val = _wtoi(value);
				Insert(&g_root, val);

				InvalidateRect(g_hWnd, NULL, false);

				return true;
			}

		case IDC_BUTTON2:
			{
				wchar_t value[10];
				GetDlgItemText(hDlg, IDC_EDIT2, value, sizeof value / sizeof(wchar_t));

				int val = _wtoi(value);
				Delete(&g_root, val);

				InvalidateRect(g_hWnd, NULL, false);

				return true;
			}

		case IDC_BUTTON3:
			{
				wchar_t value[10];
				GetDlgItemText(hDlg, IDC_EDIT3, value, sizeof value / sizeof(wchar_t));

				int val = _wtoi(value);
				Insert(&g_root, val);

				val = rand() % 500;
				SetDlgItemInt(hDlg, IDC_EDIT3, val, true);

				InvalidateRect(g_hWnd, NULL, false);

				return true;
			}

		case IDC_BUTTON4:
			{
				ReleaseByRecursive(g_root);
				g_root = nullptr;

				InvalidateRect(g_hWnd, NULL, false);

				return true;
			}
	
		default:
			break;
		}
	default:
		break;
	}

	return false;
}

LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
    switch (message)
    {
    case WM_COMMAND:
        {
            int wmId = LOWORD(wParam);
            // 메뉴 선택을 구문 분석합니다.
            switch (wmId)
            {
            case IDM_ABOUT:
                DialogBox(hInst, MAKEINTRESOURCE(IDD_ABOUTBOX), hWnd, About);
                break;
            case IDM_EXIT:
                DestroyWindow(hWnd);
                break;
            default:
                return DefWindowProc(hWnd, message, wParam, lParam);
            }
        }
        break;

	case WM_KEYDOWN:
	{
		switch (wParam)
		{
		case VK_LEFT:
			g_screen_x -= 10;
			g_screen_x = max(g_screen_x, 0);
			break;
		case VK_RIGHT:
			g_screen_x += 10;
			g_screen_x = min(3840, g_screen_x);
			break;
		case VK_UP:
			g_screen_y -= 10;
			g_screen_y = max(g_screen_y, 0);
			break;
		case VK_DOWN:
			g_screen_y += 10;
			g_screen_x = min(2160, g_screen_x);
			break;
		default:
			break;
		}
		InvalidateRect(g_hWnd, NULL, false);
	}
	break;

    case WM_PAINT:
        {
            PAINTSTRUCT ps;
            HDC hdc = BeginPaint(hWnd, &ps);

			Test3(hdc);
            
			EndPaint(hWnd, &ps);

			
        }
        break;

    case WM_DESTROY:
        PostQuitMessage(0);
        break;

	case WM_CREATE:
		if (!IsWindow(g_hMDlg))
		{
			g_hMDlg = CreateDialog(hInst, MAKEINTRESOURCE(IDD_DIALOG1), hWnd, MlessDlgProc);
			ShowWindow(g_hMDlg, SW_SHOW);
		}
		break;

    default:
        return DefWindowProc(hWnd, message, wParam, lParam);
    }
    return 0;
}

// 정보 대화 상자의 메시지 처리기입니다.
INT_PTR CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
    UNREFERENCED_PARAMETER(lParam);
    switch (message)
    {
    case WM_INITDIALOG:
        return (INT_PTR)TRUE;

    case WM_COMMAND:
        if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)
        {
            EndDialog(hDlg, LOWORD(wParam));
            return (INT_PTR)TRUE;
        }
        break;
    }

    return (INT_PTR)FALSE;
}


void InOrderTraversalByRecursive(Node* next, int depth)
{
	if (!next)
	{
		return;
	}

	InOrderTraversalByRecursive(next->_left, depth + 75);

	Ellipse(g_mem_dc, g_x, depth, g_x + 50, depth + 50);

	wchar_t data[10];
	memset(data, 0, sizeof data);
	swprintf_s(data, 10, L"%d", next->_data);
	TextOut(g_mem_dc, g_x + 15, depth + 15, data, (int)wcslen(data));

	next->_x = g_x;
	next->_y = depth;

	g_x += 30;

	InOrderTraversalByRecursive(next->_right, depth + 75);
}

void PreOrderTraversalByRecursive(Node* next)
{
	if (!next)
	{
		return;
	}

	SetBkColor(g_mem_dc, RGB(51, 184, 165));

	int balance = CalculateBalance(next);
	wchar_t balance_data[10];
	memset(balance_data, 0, sizeof balance_data);
	swprintf_s(balance_data, 10, L"%d", balance);
	TextOut(g_mem_dc, next->_x + 50, next->_y + 15, balance_data, (int)wcslen(balance_data));

	if (next->_left)
	{
		MoveToEx(g_mem_dc, next->_x + 25, next->_y + 50, NULL);
		LineTo(g_mem_dc, next->_left->_x + 25, next->_left->_y);

		PreOrderTraversalByRecursive(next->_left);
	}

	if (next->_right)
	{
		MoveToEx(g_mem_dc, next->_x + 25, next->_y + 50, NULL);
		LineTo(g_mem_dc, next->_right->_x + 25, next->_right->_y);

		PreOrderTraversalByRecursive(next->_right);
	}
}

void Test3(HDC hdc)
{
	g_x = 0;
	g_prev_x = 0;
	g_prev_y = 0;

	g_mem_dc = CreateCompatibleDC(hdc);
	HBITMAP bitmap_current = CreateCompatibleBitmap(hdc, 3840, 2160);
	HBITMAP bitmap_old = (HBITMAP)SelectObject(g_mem_dc, bitmap_current);

	HBRUSH back_color = CreateSolidBrush(RGB(51, 184, 165));
	HBRUSH old_back = (HBRUSH)SelectObject(g_mem_dc, back_color);
	Rectangle(g_mem_dc, 0, 0, 3840, 2160);

	HBRUSH circle_color = CreateSolidBrush(RGB(240, 73, 80));
	SelectObject(g_mem_dc, circle_color);

	SetTextColor(g_mem_dc, RGB(255, 255, 255));
	SetBkColor(g_mem_dc, RGB(240, 73, 80));

	HPEN pen = CreatePen(PS_SOLID, 2, RGB(255, 255, 255));
	HPEN old_pen = (HPEN)SelectObject(g_mem_dc, pen);

	DeleteDC(hdc);

	InOrderTraversalByRecursive(g_root, 0);
	PreOrderTraversalByRecursive(g_root);

	hdc = GetDC(g_hWnd);

	BitBlt(hdc, 0, 0, 1920, 1080, g_mem_dc, g_screen_x, g_screen_y, SRCCOPY);

	SelectObject(g_mem_dc, old_pen);
	SelectObject(g_mem_dc, old_back);
	DeleteDC(g_mem_dc);
}

void Insert(Node** node, DATA_TYPE data)
{
	if (!*node)
	{
		Node* new_node = new Node;
		new_node->_left = nullptr;
		new_node->_right = nullptr;
		new_node->_data = data;

		*node = new_node;

		return;
	}

	if ((*node)->_data > data)
	{
		Insert(&(*node)->_left, data);
		*node = Rebalance(*node);

	}
	else if ((*node)->_data < data)
	{
		Insert(&(*node)->_right, data);
		*node = Rebalance(*node);
	}
	else
	{

	}
}

Node* Rebalance(Node* node)
{
	int balance = CalculateBalance(node);

	if (balance > 1)
	{
		if (CalculateBalance(node->_left) > 0)
		{
			node = RotateLL(node);
		}
		else
		{
			node = RotateLR(node);
		}
	}
	else if (balance < -1)
	{
		if (CalculateBalance(node->_right) < 0)
		{
			node = RotateRR(node);
		}
		else
		{
			node = RotateRL(node);
		}
	}
	else
	{

	}

	return node;
}

void Delete(Node** node, DATA_TYPE data)
{
	Node* target = SearchNode(*node, data);
	if (!target)
	{
		return;
	}
	
	Node* deleted_node = target;
	if (deleted_node->_left)
	{
		deleted_node = deleted_node->_left;
	}

	while (1)
	{
		if (deleted_node->_right)
		{
			deleted_node = deleted_node->_right;
		}
		else
		{
			break;
		}
	}

	if (deleted_node == target)
	{
		if (deleted_node->_right)
		{
			deleted_node = deleted_node->_right;
		}

		while (1)
		{
			if (deleted_node->_left)
			{
				deleted_node = deleted_node->_left;
			}
			else
			{
				break;
			}
		}
	}

	DATA_TYPE deleted_node_data = deleted_node->_data;
	DeleteByRecursive(node, deleted_node->_data, target);
	target->_data = deleted_node_data;
}

void DeleteByRecursive(Node** node, DATA_TYPE data, Node* target)
{
	if ((*node)->_data > data)
	{
		DeleteByRecursive(&(*node)->_left, data, target);
		*node = Rebalance(*node);
	}
	else if ((*node)->_data < data)
	{
		DeleteByRecursive(&(*node)->_right, data, target);
		*node = Rebalance(*node);
	}
	else
	{
		Node* deleted_node = *node;
		*node = deleted_node->_left;

		delete deleted_node;
		
		return;
	}
}

void ReleaseByRecursive(Node* next)
{
	if (next == nullptr)
	{
		return;
	}

	ReleaseByRecursive(next->_left);
	ReleaseByRecursive(next->_right);

	delete next;
}

Node* SearchNode(Node* node, DATA_TYPE data)
{
	if (!node)
	{
		return nullptr;
	}

	if (node->_data > data)
	{
		SearchNode(node->_left, data);
	}
	else if (node->_data < data)
	{
		SearchNode(node->_right, data);
	}
	else
	{
		return node;
	}
}

/* ***************************************
		N
	  A   D
	B  C E F 
**************************************** */
Node* RotateRR(Node* node)
{
	// *node 는 N을 가리키게 되고, *node를 바꾸면 사실상 부모와 자식의 관계가 바뀌게 됨.
	Node* N = node;
	Node* D = N->_right;
	Node* E = D->_left;

	if (D && E)
	{
		D->_left = N;
		N->_right = E;

		return D;
	}

	if (D && !E)
	{
		D->_left = N;
		N->_right = nullptr;

		return D;
	}

	if (!D && E)
	{
		return N;
	}

	if (!D && !E)
	{
		return N;
	}

	assert(0);

	return nullptr;
}
 
Node* RotateLL(Node* node)
{
	// *node 는 N을 가리키게 되고, *node를 바꾸면 사실상 부모와 자식의 관계가 바뀌게 됨.
	Node* N = node;
	Node* A = N->_left;
	Node* C = A->_right;
	
	if (A && C)
	{
		A->_right = N;
		N->_left = C;
		
		return A;
	}

	if (A && !C)
	{
		A->_right = N;
		N->_left = nullptr;
		
		return A;
	}

	if (!A && C)
	{
		return N;
	}

	if (!A && !C)
	{
		return N;
	}

	assert(0);

	return nullptr;
}

Node* RotateRL(Node* node)
{
	Node* right = RotateLL(node->_right);
	node->_right = right;

	return RotateRR(node);
}

Node* RotateLR(Node* node)
{
	Node* left = RotateRR(node->_left);
	node->_left = left;

	return RotateLL(node);
}

int CalculateDepth(Node* node)
{
	int depth = 0;

	if (node)
	{
		int left_depth = CalculateDepth(node->_left);
		int right_depth = CalculateDepth(node->_right);

		depth = max(left_depth, right_depth) + 1;
	}

	return depth;
}

int CalculateBalance(Node* node)
{
	int left_depth = CalculateDepth(node->_left);
	int right_depth = CalculateDepth(node->_right);

	return left_depth - right_depth;
}
