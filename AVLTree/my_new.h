#ifndef  __MEMORY_LEAK_CHECKER__
#define __MEMORY_LEAK_CHECKER__

/* (2017-10-28) TODO :
	현재 문제점은 파괴자가 있는 클래스에서 존재한다.
	예를 들어 AAA* a = new AAA 라고 한뒤 delete[] a 라고 하면
	파괴자가 호출되고, 다시 파괴자에서 delete를 호출하고 
	delete에서 파괴자를 다시 호출 하는 무한루프에 빠져버림.
	참고 : https://stackoverflow.com/questions/7850374/stuck-in-infinite-loop-in-deallocating-memory
	(2017-10-28-2)
	단 무한루프가 아닐지도
	파괴자가 있는 클래스에서 new를 했을경우 클래스 객체 바로 앞에 얼마나 했는지 써있는데
	여기에 만약 fdfdfdfdfd...가 써있다면 무한루프급이네? 이런경우인가?
	해결은 못하였음...
	(2017-10-28-3)
	-2의 문제가 맞았음. 해당 값은 0x00000000fdfdfdfd로 되어있는데 이 값을 1로 바꿔보니 한 번 호출됨.
*/
void* operator new (size_t size, char *File, int Line);
void* operator new[](size_t size, char *File, int Line);

//*********************************************
// 다음 delete 는 실제로 쓰진 않지만 문법상 컴파일 오류를 막기 위해 만듬
// 속은 비워둠.
// 그런데, vs2015에서는 없어도 컴파일 ok, 그러나 일단 넣어둠.
//*********************************************
void operator delete (void * p, char *File, int Line);
void operator delete[](void * p, char *File, int Line);

//*********************************************
// 실제로 사용할 delete.
//*********************************************
void operator delete (void* p);
void operator delete[](void * p);


#define new new(__FILE__, __LINE__)
#endif // ! 


