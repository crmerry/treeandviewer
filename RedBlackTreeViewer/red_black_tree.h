#pragma once

namespace RED_BLACK_TREE
{
	typedef int DATA_TYPE;

	struct Node
	{
		enum eColor
		{
			Black = 0,
			Red
		};

		Node*		_parent	= nullptr;
		Node*		_left	= nullptr;
		Node*		_right	= nullptr;
		eColor		_color = eColor::Red;
		DATA_TYPE	_data;
		int			_x;
		int			_y;
	};

	class RedBlackTree
	{
	public:
		RedBlackTree();
		~RedBlackTree();
		bool InsertNode(DATA_TYPE data);
		void PreorderPrint();
		void InorderPrint();
		void PostorderPrint();
		void Release();
		bool Search(Node** const node, DATA_TYPE data);
		bool Delete(DATA_TYPE data);
		void RotateTest(int direction, DATA_TYPE data);
		Node* GetRoot() const;
		Node* GetLeft(const Node* const node) const;
		Node* GetRight(const Node* const node) const;
		Node* GetNil() const;

	protected:
		Node* CreateNode(DATA_TYPE data);
		bool InsertNodeKeepBalance(Node* const node);
		void RotateRight(Node* const node);
		void RotateLeft(Node* const node);
		void PreorderPrintByRecursive(const Node* const next, int depth = 0) const;
		void InorderPrintByRecursive(const Node* const next, int depth = 0) const;
		void PostorderPrintByRecursive(const Node* const next, int depth = 0) const;
		void PrintDataByDepth(const Node* const next, int depth) const;
		void ReleaseByRecursive(Node* const next);
		bool SearchByRecursive(Node** const target, Node** const prev, Node* const next, DATA_TYPE data);
		bool DeleteByLoop(Node* target);

	private:
		Node*	_root;
		Node*	_nil;
	};
}
