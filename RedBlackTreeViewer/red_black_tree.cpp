#include "stdafx.h"
#include "red_black_tree.h"

using namespace RED_BLACK_TREE;

RedBlackTree::RedBlackTree()
	:_root(nullptr)
{
	_nil = new Node;
	_nil->_color = Node::eColor::Black;
}

RedBlackTree::~RedBlackTree()
{
	ReleaseByRecursive(_root);
	_root = nullptr;
	delete _nil;
}

bool RedBlackTree::InsertNode(DATA_TYPE data)
{
	Node* new_node = CreateNode(data);

	if (InsertNodeKeepBalance(new_node))
	{
		return true;
	}
	else
	{
		delete new_node;

		return false;
	}
}

void RedBlackTree::PreorderPrint()
{
	PreorderPrintByRecursive(_root);
}

void RedBlackTree::InorderPrint()
{
	InorderPrintByRecursive(_root);
}

void RedBlackTree::PostorderPrint()
{
	PostorderPrintByRecursive(_root);
}

void RedBlackTree::Release()
{
	ReleaseByRecursive(_root);
	_root = nullptr;
}

bool RedBlackTree::Search(Node** const target, DATA_TYPE data)
{
	Node* prev;

	return SearchByRecursive(target, &prev, _root, data);
}

bool RedBlackTree::Delete(DATA_TYPE data)
{
	Node* target = nullptr;
	Node* prev = nullptr;

	if (SearchByRecursive(&target, &prev, _root, data))
	{
		if (DeleteByLoop(target))
		{
			if (_root)
			{
				_root->_color = Node::eColor::Black;
			}
		}
	}
	else
	{
		return false;
	}
}

void RedBlackTree::RotateTest(int direction, DATA_TYPE data)
{
	Node* target = nullptr;
	Node* parent = nullptr;

	if (!SearchByRecursive(&target, &parent, _root, data))
	{
		return;
	}

	if (direction == 0)
	{
		RotateLeft(target);
	}
	else
	{
		RotateRight(target);
	}
}

Node* RedBlackTree::GetRoot() const
{
	return _root;
}

Node* RedBlackTree::GetLeft(const Node* const node) const
{
	return node->_left;
}

Node* RedBlackTree::GetRight(const Node* const node) const
{
	return node->_right;
}

Node* RED_BLACK_TREE::RedBlackTree::GetNil() const
{
	return _nil;
}

Node* RedBlackTree::CreateNode(DATA_TYPE data)
{
	Node* created_node = new Node;
	created_node->_data = data;

	return created_node;
}

bool RED_BLACK_TREE::RedBlackTree::InsertNodeKeepBalance(Node* const node)
{
	if (!_root)
	{
		_root = node;
		_root->_color = Node::eColor::Black;
		_root->_left = _nil;
		_root->_right = _nil;

		return true;
	}

	DATA_TYPE data = node->_data;
	Node* N = _root;
	Node* P = nullptr;
	Node* GP = nullptr;
	Node* U = nullptr;

	while (1)
	{
		if (N->_data < data)
		{
			P = N;
			if ((N = N->_right) == _nil)
			{
				N = node;

				P->_right = N;
				N->_parent = P;

				N->_left = _nil;
				N->_right = _nil;

				break;
			}
		}
		else if (N->_data > data)
		{
			P = N;
			if ((N = N->_left) == _nil)
			{
				N = node;

				P->_left = N;
				N->_parent = P;

				N->_left = _nil;
				N->_right = _nil;

				break;
			}
		}
		else
		{
			return false;
		}
	}

	while (1)
	{	
		if (!(GP = P->_parent))
		{
			break;
		}

		if (GP->_left == P)
		{
			U = GP->_right;
		}
		else if (GP->_right == P)
		{
			U = GP->_left;
		}
		else
		{
			assert(0);
		}

		if (P->_color == Node::eColor::Red && U->_color == Node::eColor::Red)
		{
			P->_color = Node::eColor::Black;
			U->_color = Node::eColor::Black;
			GP->_color = Node::eColor::Red;
		}
		else
		{
			break;
		}

		N = GP;
		if (!(P = N->_parent))
		{
			GP = nullptr;

			break;
		}
	}

	if (GP && N->_color == Node::eColor::Red && P->_color == Node::eColor::Red && U->_color == Node::eColor::Black)
	{
		if (GP->_left == P)
		{
			if (P->_right == N)
			{
				RotateLeft(P);

				Node* temp = P;
				P = N;
				N = temp;
			}

			if (P->_left == N)
			{
				P->_color = Node::eColor::Black;
				GP->_color = Node::eColor::Red;

				RotateRight(GP);
			}
		}

		
		if (GP->_right == P)
		{
			if (P->_left == N)
			{
				RotateRight(P);

				Node* temp = P;
				P = N;
				N = temp;
			}

			if (P->_right == N)
			{
				P->_color = Node::eColor::Black;
				GP->_color = Node::eColor::Red;

				RotateLeft(GP);
			}
		}
	}

	_root->_color = Node::eColor::Black;

	return true;
}

/* ***************************************
			P
		N
	A
		C
**************************************** */
void RedBlackTree::RotateRight(Node* const node)
{
	Node* A = node->_left;
	Node* P = nullptr;
	Node* C = nullptr;

	if (A != _nil)
	{
		C = A->_right;
	}
	else
	{
		/* ***************************************
			A가 nil 노드인 경우, N을 기준으로
			회전하는 것이 의미 없음.
		**************************************** */
		return;
	}
	
	if (P = node->_parent)
	{
		if (P->_left == node)
		{
			P->_left = A;
		}
		else if (P->_right == node)
		{
			P->_right = A;
		}
		else
		{
			assert(0);
		}
	}

	if (C != _nil)
	{
		C->_parent = node;
	}

	node->_left = C;
	node->_parent = A;

	A->_right = node;
	A->_parent = P;

	if (node == _root)
	{
		_root = A;
	}
}

void RedBlackTree::RotateLeft(Node* const node)
{
	Node* D = node->_right;
	Node* P = nullptr; // node->_parent;
	Node* E = nullptr;

	if (D != _nil)
	{
		E = D->_left;
	}
	else
	{
		return;
	}

	if (P = node->_parent)
	{
		if (P->_left == node)
		{
			P->_left = D;
		}
		else if (P->_right == node)
		{
			P->_right = D;
		}
		else
		{
			assert(0);
		}
	}

	if (E != _nil)
	{
		E->_parent = node;
	}

	node->_right = E;
	node->_parent = D;

	D->_parent = P;
	D->_left = node;

	if (node == _root)
	{
		_root = D;
	}
}

void RedBlackTree::PreorderPrintByRecursive(const Node* const next, int depth) const
{
	if (next == _nil)
	{
		return;
	}

	PrintDataByDepth(next, depth);
	PreorderPrintByRecursive(next->_left, depth + 1);
	PreorderPrintByRecursive(next->_right, depth + 1);
}

void RedBlackTree::InorderPrintByRecursive(const Node* const next, int depth) const
{
	if (next == _nil)
	{
		return;
	}

	InorderPrintByRecursive(next->_left, depth + 1);
	PrintDataByDepth(next, depth);
	InorderPrintByRecursive(next->_right, depth + 1);
}

void RedBlackTree::PostorderPrintByRecursive(const Node* const next, int depth) const
{
	if (next == _nil)
	{
		return;
	}

	PostorderPrintByRecursive(next->_left, depth + 1);
	PostorderPrintByRecursive(next->_right, depth + 1);
	PrintDataByDepth(next, depth);
}


void RedBlackTree::PrintDataByDepth(const Node* const next, int depth) const
{
	for (int index = 0; index < depth; index++)
	{
		printf(" ");
	}
	printf("%d\n", next->_data);
}

void RedBlackTree::ReleaseByRecursive(Node* const next)
{
	if (next == _nil || next == nullptr)
	{
		return;
	}

	ReleaseByRecursive(next->_left);
	ReleaseByRecursive(next->_right);

	delete next;
}

bool RedBlackTree::SearchByRecursive(Node** const target, Node** const prev, Node* const next, DATA_TYPE data)
{
	if (next == _nil)
	{
		return false;
	}

	if (next->_data > data)
	{
		if (SearchByRecursive(target, prev, next->_left, data))
		{
			if (!*prev)
			{
				*prev = next;
			}

			return true;
		}
	}
	else if (next->_data < data)
	{
		if (SearchByRecursive(target, prev, next->_right, data))
		{
			if (!*prev)
			{
				*prev = next;
			}

			return true;
		}
	}
	else
	{
		*target = next;

		return true;
	}

	return false;
}

bool RedBlackTree::DeleteByLoop(Node* target)
{
	Node* deleted_node = target;
	Node* replace_node = nullptr;
	Node* parent_node = nullptr;
	Node* S = nullptr;

	if (deleted_node->_right != _nil)
	{
		deleted_node = deleted_node->_right;

		while (1)
		{
			if (deleted_node->_left != _nil)
			{
				deleted_node = deleted_node->_left;
			}
			else
			{
				break;
			}
		}
	}
	
	if (deleted_node->_right == _nil && deleted_node->_left != _nil)
	{
		deleted_node = deleted_node->_left;
	}
	/* ***************************************
		위 코드 까지, 지울 노드가 정해짐.
	**************************************** */

	/* ***************************************
		지울 노드, 대체 노드, 부모 노드, 형제 노드를 
		정리 & 밸런스를 맞춤.
	**************************************** */
	if (deleted_node == _root)
	{
		delete deleted_node;
		_root = nullptr;

		return true;
	}

	target->_data = deleted_node->_data;
	replace_node = deleted_node->_right;
	parent_node = deleted_node->_parent;
	replace_node->_parent = parent_node;

	if (parent_node->_left == deleted_node)
	{
		parent_node->_left = replace_node;
	}
	else if (parent_node->_right == deleted_node)
	{
		parent_node->_right = replace_node;
	}
	else
	{
		assert(0);
	}

	if (deleted_node->_color == Node::eColor::Red)
	{
		delete deleted_node;

		return true;
	}

	delete deleted_node;
	/* ***************************************
		위 코드까지, deleted 노드는 정리되고,
		대체 노드가 deleted 노드에 자리에 들어감.
	**************************************** */

	/* ***************************************
		이후, 밸런스를 잡는 반복 작업
	**************************************** */
	while (1)
	{
		if (replace_node == parent_node->_left)
		{
			S = parent_node->_right;

			if (replace_node->_color == Node::eColor::Red)
			{
				replace_node->_color = Node::eColor::Black;

				return true;
			}

			if (S->_color == Node::eColor::Red)
			{
				parent_node->_color = Node::eColor::Red;
				S->_color = Node::eColor::Black;
				RotateLeft(parent_node);

				continue;
			}

			if (S->_color == Node::eColor::Black && S->_left->_color == Node::eColor::Black && S->_right->_color == Node::eColor::Black)
			{
				S->_color = Node::eColor::Red;

				replace_node = parent_node;
				if (replace_node == _root)
				{
					return true;
				}

				parent_node = replace_node->_parent;

				continue;
			}

			if (S->_color == Node::eColor::Black && S->_left->_color == Node::eColor::Red)
			{
				Node* LC = S->_left;
				LC->_color = Node::eColor::Black;
				S->_color = Node::eColor::Red;

				RotateRight(S);

				continue;
			}

			if (S->_color == Node::eColor::Black && S->_right->_color == Node::eColor::Red)
			{
				S->_color = parent_node->_color;
				parent_node->_color = Node::eColor::Black;
				S->_right->_color = Node::eColor::Black;

				RotateLeft(parent_node);

				return true;
			}
		}

		if (replace_node == parent_node->_right)
		{
			S = parent_node->_left;

			if (replace_node->_color == Node::eColor::Red)
			{
				replace_node->_color = Node::eColor::Black;

				return true;
			}

			if (S->_color == Node::eColor::Red)
			{
				parent_node->_color = Node::eColor::Red;
				S->_color = Node::eColor::Black;
				RotateRight(parent_node);

				continue;
			}

			if (S->_color == Node::eColor::Black && S->_right->_color == Node::eColor::Black && S->_left->_color == Node::eColor::Black)
			{
				S->_color = Node::eColor::Red;

				replace_node = parent_node;
				if (replace_node == _root)
				{
					return true;
				}

				parent_node = replace_node->_parent;

				continue;
			}

			if (S->_color == Node::eColor::Black && S->_right->_color == Node::eColor::Red)
			{
				Node* RC = S->_right;
				RC->_color = Node::eColor::Black;
				S->_color = Node::eColor::Red;

				RotateLeft(S);

				continue;
			}

			if (S->_color == Node::eColor::Black && S->_left->_color == Node::eColor::Red)
			{
				S->_color = parent_node->_color;
				parent_node->_color = Node::eColor::Black;
				S->_left->_color = Node::eColor::Black;

				RotateRight(parent_node);

				return true;
			}
		}
	}

	return false;
}
