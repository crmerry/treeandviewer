#pragma once

namespace BINARY_TREE
{
	typedef int DATA_TYPE;
	struct Node
	{
		Node*		_left = nullptr;
		Node*		_right = nullptr;
		DATA_TYPE	_data;
	};

	class BinaryTree
	{
	public:
		BinaryTree();
		~BinaryTree();
		bool InsertNode(DATA_TYPE data);
		bool InsertNode2(DATA_TYPE data);
		void PreorderPrint();
		void InorderPrint();
		void PostorderPrint();
		void Release();
		bool Search(Node** target, DATA_TYPE data);
		void Delete(DATA_TYPE data);
	protected:
		Node* CreateNode(DATA_TYPE data);
		bool LinkNodeToNodeByRecursive(Node** parent_node, Node* new_node);
		bool LinkNodeToNodeByRecursive2(Node* parent_node, Node* new_node);
		void PreorderPrintByRecursive(Node* next, int depth = 0);
		void InorderPrintByRecursive(Node* next, int depth = 0);
		void PostorderPrintByRecursive(Node* next, int depth = 0);
		void PrintDataByDepth(Node* next, int depth);
		void ReleaseByRecursive(Node* next);
		bool SearchByRecursive(Node* target, DATA_TYPE data);
		void DeleteByRecurisve(DATA_TYPE data);
	private:
		Node* _root;
	};
}
