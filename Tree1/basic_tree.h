#pragma once
namespace BASIC_TREE
{
	struct TreeNode
	{
		struct TreeNode* _child;
		struct TreeNode* _sibling;
		int _data;
		int _x;
		int _y;
	};

	TreeNode*	CreateNode(int data);  // 트리 노드를 동적 생성하고 초기화 된 노드를 리턴 합니다.
	void		DeleteNode(TreeNode* node); // 인자로 입력 받은 노드를 delete 합니다. 
	void		AddChildNode(TreeNode* parent, TreeNode* child);  // 부모 노드를 인자로 받아 해당 노드의 자식으로 붙입니다.
	void		PrintTree(TreeNode* node, int depth);
	void		ReleaseTree(TreeNode* root);			   // 전체 노드를 삭제 (delete) 
}