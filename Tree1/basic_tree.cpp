#include "basic_tree.h"
#include <stdio.h>

using namespace  BASIC_TREE;

TreeNode* BASIC_TREE::CreateNode(int data)
{
	TreeNode* node = new TreeNode();

	node->_child = nullptr;
	node->_sibling = nullptr;
	node->_data = data;

	return node;
}

void BASIC_TREE::DeleteNode(TreeNode* node)
{
	delete node;
}

void BASIC_TREE::AddChildNode(TreeNode* parent, TreeNode* child)
{
	TreeNode* next_node = parent->_child;
	parent->_child = child;
	child->_sibling = next_node;
}

void BASIC_TREE::PrintTree(TreeNode* node, int depth)
{
	if (node == nullptr)
	{
		return;
	}

	for (int index = 0; index < depth; index++)
	{
		printf(" ");
	}
	printf("%d\n", node->_data);

	PrintTree(node->_child, depth + 1);
	PrintTree(node->_sibling, depth);
}

void BASIC_TREE::ReleaseTree(TreeNode* root)
{
	if (root == nullptr)
	{
		return;
	}

	//ReleaseTree(root->_sibling);
	//ReleaseTree(root->_child);

	ReleaseTree(root->_child);
	ReleaseTree(root->_sibling);

	printf("%d delete \n", root->_data);
	DeleteNode(root);
}