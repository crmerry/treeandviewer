#include <assert.h>
#include <stdio.h>
#include "binary_tree.h"

using namespace BINARY_TREE;

BinaryTree::BinaryTree()
	:_root(nullptr)
{

}

BinaryTree::~BinaryTree()
{
	ReleaseByRecursive(_root);
	_root = nullptr;
}

bool BinaryTree::InsertNode(DATA_TYPE data)
{
	Node* new_node = CreateNode(data);

	if (!_root)
	{
		_root = new_node;

		return true;
	}

	if (!LinkNodeToNodeByRecursive(&_root, new_node))
	{
		delete new_node;

		return false;
	}

	return true;
}

bool BINARY_TREE::BinaryTree::InsertNode2(DATA_TYPE data)
{
	Node* new_node = CreateNode(data);

	if (!_root)
	{
		_root = new_node;

		return true;
	}

	if (!LinkNodeToNodeByRecursive2(_root, new_node))
	{
		delete new_node;

		return false;
	}

	return true;
}

void BinaryTree::PreorderPrint()
{
	PreorderPrintByRecursive(_root);
}

void BinaryTree::InorderPrint()
{
	InorderPrintByRecursive(_root);
}

void BinaryTree::PostorderPrint()
{
	PostorderPrintByRecursive(_root);
}

void BinaryTree::Release()
{
	ReleaseByRecursive(_root);
	_root = nullptr;
}

Node* BinaryTree::CreateNode(DATA_TYPE data)
{
	Node* created_node = new Node;
	created_node->_data = data;

	return created_node;
}

/* ***************************************
	LinkNodeToNodeByRecursive는 
	Node**을 이용하는 방식과 Node*을 이용하는 
	두 가지 방식으로 했는데,
	큰 차이는 아니지만, Node*가 조금 더 빠르다.
**************************************** */
bool BinaryTree::LinkNodeToNodeByRecursive(Node** parent_node, Node* new_node)
{
	if (!(*parent_node))
	{
		*parent_node = new_node;

		return true;
	}

	if ((*parent_node)->_data > new_node->_data)
	{
		return LinkNodeToNodeByRecursive(&(*parent_node)->_left, new_node);
	}
	else if ((*parent_node)->_data < new_node->_data)
	{
		return LinkNodeToNodeByRecursive(&(*parent_node)->_right, new_node);
	}
	else
	{
		assert(0);

		return false;
	}
}

bool BINARY_TREE::BinaryTree::LinkNodeToNodeByRecursive2(Node* parent_node, Node* new_node)
{
	if (!parent_node)
	{
		return true;
	}

	if (parent_node->_data > new_node->_data)
	{
		if (!LinkNodeToNodeByRecursive2(parent_node->_left, new_node))
		{
			return false;
		}

		if (!parent_node->_left)
		{
			parent_node->_left = new_node;

			return true;
		}
	}
	else if (parent_node->_data < new_node->_data)
	{
		if (!LinkNodeToNodeByRecursive2(parent_node->_right, new_node))
		{
			return false;
		}

		if (!parent_node->_right)
		{
			parent_node->_right = new_node;

			return true;
		}
	}
	else
	{
		assert(0);

		return false;
	}

	return false;
}

void BinaryTree::PreorderPrintByRecursive(Node* next, int depth)
{
	if (!next)
	{
		return;
	}

	PrintDataByDepth(next, depth);
	PreorderPrintByRecursive(next->_left, depth + 1);
	PreorderPrintByRecursive(next->_right, depth + 1);
}

void BinaryTree::InorderPrintByRecursive(Node* next, int depth)
{
	if (!next)
	{
		return;
	}

	InorderPrintByRecursive(next->_left, depth + 1);
	PrintDataByDepth(next, depth);
	InorderPrintByRecursive(next->_right, depth + 1);
}

void BinaryTree::PostorderPrintByRecursive(Node* next, int depth)
{
	if (!next)
	{
		return;
	}

	PostorderPrintByRecursive(next->_left, depth + 1);
	PostorderPrintByRecursive(next->_right, depth + 1);
	PrintDataByDepth(next, depth);
}

void BinaryTree::PrintDataByDepth(Node* next, int depth)
{
	for (int index = 0; index < depth; index++)
	{
		printf(" ");
	}
	printf("%d\n", next->_data);
}

void BinaryTree::ReleaseByRecursive(Node* next)
{
	if (next == nullptr)
	{
		return;
	}

	ReleaseByRecursive(next->_left);
	ReleaseByRecursive(next->_right);

	delete next;
}