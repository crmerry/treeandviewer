#include <stdio.h>
#include <iostream>
#include <stdlib.h>
#include <assert.h>
#include <math.h>
#include <exception>
#include "basic_tree.h"
#include "binary_tree.h"
#include "Profiler.h"

void TreeTest1()
{
	using namespace BASIC_TREE;

	TreeNode *pAddNode_R, *pAddNode_N;
	TreeNode *pRoot = CreateNode(0);

	pAddNode_R = CreateNode(1);
	AddChildNode(pRoot, pAddNode_R);

	pAddNode_N = CreateNode(11);
	AddChildNode(pAddNode_R, pAddNode_N);
	pAddNode_N = CreateNode(12);
	AddChildNode(pAddNode_R, pAddNode_N);
	pAddNode_N = CreateNode(13);
	AddChildNode(pAddNode_R, pAddNode_N);
	pAddNode_N = CreateNode(14);
	AddChildNode(pAddNode_R, pAddNode_N);
	pAddNode_N = CreateNode(15);
	AddChildNode(pAddNode_R, pAddNode_N);


	pAddNode_R = CreateNode(2);
	AddChildNode(pRoot, pAddNode_R);

	pAddNode_N = CreateNode(21);
	AddChildNode(pAddNode_R, pAddNode_N);
	pAddNode_N = CreateNode(22);
	AddChildNode(pAddNode_R, pAddNode_N);
	pAddNode_N = CreateNode(23);
	AddChildNode(pAddNode_R, pAddNode_N);
	pAddNode_N = CreateNode(24);
	AddChildNode(pAddNode_R, pAddNode_N);
	pAddNode_N = CreateNode(25);
	AddChildNode(pAddNode_R, pAddNode_N);


	pAddNode_R = CreateNode(3);
	AddChildNode(pRoot, pAddNode_R);

	pAddNode_N = CreateNode(31);
	AddChildNode(pAddNode_R, pAddNode_N);
	pAddNode_N = CreateNode(32);
	AddChildNode(pAddNode_R, pAddNode_N);
	pAddNode_N = CreateNode(33);
	AddChildNode(pAddNode_R, pAddNode_N);
	pAddNode_N = CreateNode(34);
	AddChildNode(pAddNode_R, pAddNode_N);
	pAddNode_N = CreateNode(35);
	AddChildNode(pAddNode_R, pAddNode_N);

	PrintTree(pRoot, 0);

	ReleaseTree(pRoot);
}

void TreeTest2()
{
	using namespace BINARY_TREE;

	BinaryTree Tree;

	Tree.InsertNode(10);
	Tree.InsertNode(11);
	Tree.InsertNode(12);
	Tree.InsertNode(5);
	Tree.InsertNode(6);
	Tree.InsertNode(3);
	Tree.InsertNode(9);
	Tree.InsertNode(100);
	Tree.InsertNode(20);
	Tree.InsertNode(1);
	Tree.InsertNode(2);
	Tree.InsertNode(4);
	Tree.InsertNode(7);
	Tree.InsertNode(8);
	Tree.InsertNode(111);
	Tree.InsertNode(112);
	Tree.InsertNode(101);
	Tree.InsertNode(24);
	Tree.InsertNode(23);
	Tree.InsertNode(22);
	Tree.InsertNode(25);
	Tree.InsertNode(26);

	printf("pre-order \n");
	Tree.PreorderPrint();	// 전위순회 출력

	printf("in-order \n");
	Tree.InorderPrint();	// 중위순회 출력

	printf("post-order \n");
	Tree.PostorderPrint();	// 후위순회 출력

	Tree.Release();

	printf("pre-order \n");
	Tree.PreorderPrint();	// 전위순회 출력

	printf("in-order \n");
	Tree.InorderPrint();	// 중위순회 출력

	printf("post-order \n");
	Tree.PostorderPrint();	// 후위순회 출력
}

void TreeTest3()
{
	using namespace BINARY_TREE;

	BinaryTree Tree;

	Tree.InsertNode2(10);
	Tree.InsertNode2(11);
	Tree.InsertNode2(12);
	Tree.InsertNode2(5);
	Tree.InsertNode2(6);
	Tree.InsertNode2(3);
	Tree.InsertNode2(9);
	Tree.InsertNode2(100);
	Tree.InsertNode2(20);
	Tree.InsertNode2(1);
	Tree.InsertNode2(2);
	Tree.InsertNode2(4);
	Tree.InsertNode2(7);
	Tree.InsertNode2(8);
	Tree.InsertNode2(111);
	Tree.InsertNode2(112);
	Tree.InsertNode2(101);
	Tree.InsertNode2(24);
	Tree.InsertNode2(23);
	Tree.InsertNode2(22);
	Tree.InsertNode2(25);
	Tree.InsertNode2(26);

	printf("pre-order \n");
	Tree.PreorderPrint();	// 전위순회 출력

	printf("in-order \n");
	Tree.InorderPrint();	// 중위순회 출력

	printf("post-order \n");
	Tree.PostorderPrint();	// 후위순회 출력

	Tree.Release();

	printf("pre-order \n");
	Tree.PreorderPrint();	// 전위순회 출력

	printf("in-order \n");
	Tree.InorderPrint();	// 중위순회 출력

	printf("post-order \n");
	Tree.PostorderPrint();	// 후위순회 출력
}

void Test1()
{
	/* ***************************************
		 BinaryTree의 insert 구현 속도 비교
	**************************************** */
	const int node_count = 1000000;

	BINARY_TREE::BinaryTree binary_tree;
	BINARY_TREE::BinaryTree binary_tree2;

	PROFILE_INITIALIZE;

	for (int index = 0; index < node_count; index++)
	{
		int data = rand() % 1000;
		PROFILE_BEGIN(L"1번");
		binary_tree.InsertNode(data);
		PROFILE_END(L"1번");
	}

	for (int index = 0; index < node_count; index++)
	{
		int data = rand() % 1000;
		PROFILE_BEGIN(L"2번");
		binary_tree2.InsertNode2(data);
		PROFILE_END(L"2번");
	}

	PROFILE_OUT;
}

void perverted()
{
	try {
		throw std::exception();
	}
	catch (std::exception& e)
	{
		perverted();
		std::cout << e.what();
	}
}

class Window
{
public:
	virtual void set_color() = 0;
	virtual void prompt() = 0;

private:

};

class Window_with_menu : public virtual Window
{
public:
	void prompt()
	{

	}
private:

};

class Window_with_border : public virtual Window
{
public:
	void set_color()
	{

	}
private:

};

class My_Window : public Window_with_border, public Window_with_menu
{
public:

private:
};


class A
{
protected:
	char a[128];
};

class B : public A
{
public:
	void Test(B* in_b)
	{
		a[0] = 'a';
		in_b->a[20] = 'c';
	}
};
void main()
{
	My_Window w;

	B _b; 
	B b;
	b.Test(&_b);
}